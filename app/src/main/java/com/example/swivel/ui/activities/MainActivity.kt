package com.example.swivel.ui.activities

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import butterknife.BindView
import butterknife.ButterKnife
import com.example.swivel.R
import com.example.swivel.dao.Article
import com.example.swivel.ui.fragments.detail.DetailFragment
import com.example.swivel.ui.fragments.headline.HeadlinesFragment
import com.example.swivel.ui.fragments.preferred.PreferredFragment
import com.example.swivel.ui.fragments.profile.ProfileFragment
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener, HeadlinesFragment.FragmentInteractionListener,
    PreferredFragment.FragmentInteractionListener {

    @BindView(R.id.nav_view)
    lateinit var navView: BottomNavigationView

    @BindView(R.id.toolbar)
    lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        setSupportActionBar(toolbar)
        navView.setOnNavigationItemSelectedListener(this)
        navView.selectedItemId = R.id.navigation_headlines
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.navigation_headlines -> {
                loadFragment(HeadlinesFragment(), "Headlines")
            }
            R.id.navigation_preferred -> {
                loadFragment(PreferredFragment(), "Preferred")
            }
            R.id.navigation_profile -> {
                loadFragment(ProfileFragment(), "Profile")
            }
        }
        item.isChecked = true
        return false
    }

    private fun loadFragment(fragment: Fragment, title: String) {
        toolbar.title = title
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.frame_container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    override fun onArticleSelected(article: Article) {
        val fragment = DetailFragment.newInstance(article)
        fragment.show(supportFragmentManager, fragment.tag)
    }
}
